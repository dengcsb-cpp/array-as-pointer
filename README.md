

## Array as Pointer

Create a function which accepts an integer array in the form of a pointer as a parameter to the function. This function will fill the array with random values.

Use int* numbers instead of int numbers[]

## Creating a Dynamic Array

Create a function which creates an integer array internally and fill that array with random values. This function will return the created array back to the caller.

 - An array can be defined as a pointer to the first element.
   
  - Arrays cannot be used as a return type. However, a pointer to the
   array is allowed.

## Deleting dynamic elements of a dynamic array

Create a function similar to the first task but each element is a dynamically allocated integer. After calling the function, the array and all its elements must be deleted properly.